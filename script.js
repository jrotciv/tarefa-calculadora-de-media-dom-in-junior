function verificarValidade(valor){
    if (valor == ""){
        alert("Por favor, insira uma nota")
        return 0
    }
    else if (valor < 0 || 10 < valor){
        alert("A nota digitada é inválida, por favor, insira uma nota válida.")
        return 0
    }
    else if (isNaN(parseFloat(valor))){
        alert("A nota digitada é inválida, por favor, insira uma nota válida.")
        return 0
    }
    return 1
}

function substituirVirgula(valor){
    if ((valor.match(/,/g) || []).length == 1){
        valor = valor.replace(",", ".")

        return valor
    }

    return valor
}

function adicionarNota(){
    let input = document.querySelector(".nota")

    input.value = substituirVirgula(input.value)
    let validade = verificarValidade(input.value)

    if (validade == 1){
        let texto = document.createElement("option")
        texto.innerText = `A nota ${posicao_nota} foi ${input.value}`
        notas.push(parseFloat(input.value))
        armazenamento.append(texto)
        input.value = ""
        posicao_nota++
    }
    else{
        input.value = "" 
    }
}

function calcularMedia(){
    let soma = 0
    notas.forEach((nota) =>{
        soma += nota
    })

    let media = soma/notas.length

    let mostrarMedia = document.querySelector(".media")
    mostrarMedia.innerText = `A média é: ${media.toFixed(2)}`
}


let notas = []

let armazenamento = document.querySelector("#notasArmazenadas")

let posicao_nota = 1

let btn_adicionar = document.querySelector(".btn_adicionar")
btn_adicionar.addEventListener("click", adicionarNota)

let btn_calcular = document.querySelector(".calcular_media")
btn_calcular.addEventListener("click", calcularMedia)